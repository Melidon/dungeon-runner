#include "move.h"

static void calculate_collision(Character *character, GameState *gamestate)
{
    /* TODO DON'T CRASH OUTSIDE OF THE MAP */
    if(true)
    {
        /* wall on the left */
        if(!gamestate->current_map[(int) (character->position.y+0.05)][(int) (character->position.x+0.2)].walkable || !gamestate->current_map[(int) (character->position.y+1-0.05)][(int) (character->position.x+0.2)].walkable)
        {
            if(character->movement.x < 0)
            {
                character->movement.x = 0;
            }
        }

        /* wall on the right */
        if(!gamestate->current_map[(int) (character->position.y+0.05)][(int) (character->position.x+1-0.2)].walkable || !gamestate->current_map[(int) (character->position.y+1-0.05)][(int) (character->position.x+1-0.2)].walkable)
        {
            if(character->movement.x > 0)
            {
                character->movement.x = 0;
            }
        }

        /* wall above */
        if(!gamestate->current_map[(int) (character->position.y)][(int) (character->position.x+0.25)].walkable || !gamestate->current_map[(int) (character->position.y)][(int) (character->position.x+1-0.25)].walkable)
        {
            if(character->movement.y < 0)
            {
                character->movement.y = 0;
            }
        }

        /* wall underneath */
        if(!gamestate->current_map[(int) (character->position.y+1)][(int) (character->position.x+0.25)].walkable || !gamestate->current_map[(int) (character->position.y+1)][(int) (character->position.x+1-0.25)].walkable)
        {
            if(character->movement.y > 0)
            {
                character->movement.y = 0;
            }
        }
    }
}

static void select_faceing_direction(Character *character)
{
    if(character->movement.x > 0)
    {
        character->faceing_right = true;
    }
    if(character->movement.x < 0)
    {
        character->faceing_right = false;
    }
}

static void calculate_new_position(Character *character)
{
    /* actually change the coordinates */
    character->position.x += (character->speed) * (character->movement.x) / 500;
    character->position.y += (character->speed) * (character->movement.y) / 500;
}

void move(Character *character, GameState *gamestate)
{
    if(character->animation_type == WALK)
    {
        /* It is necessary, unless you want to go through walls */
        calculate_collision(character, gamestate);

        /* pretty obvious */
        select_faceing_direction(character);

        /* trivial */
        calculate_new_position(character);
    }
}

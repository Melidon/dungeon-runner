#ifndef LOAD_MAP_H_INCLUDED
#define LOAD_MAP_H_INCLUDED

#include "data.h"

void load_map(GameState *gamestate, const GameData *gamedata);

#endif // LOAD_MAP_H_INCLUDED

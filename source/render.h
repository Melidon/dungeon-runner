#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED

#include "data.h"

void render(const GameData *gamedata, GameState *gamestate, Graphics *graphics);

#endif // RENDER_H_INCLUDED

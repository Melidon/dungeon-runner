#ifndef GAME_LOGIC_H_INCLUDED
#define GAME_LOGIC_H_INCLUDED

#include "data.h"

void logic(const UserInput *userinput, const GameData *gamedata, GameState *gamestate);

#endif // GAME_LOGIC_H_INCLUDED

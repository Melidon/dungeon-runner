#ifndef CALCULATE_DAMAGE_H_INCLUDED
#define CALCULATE_DAMAGE_H_INCLUDED

#include "data.h"

void calculate_damage(Character *mob, Character *player, const GameData *gamedata);

#endif // CALCULATE_DAMAGE_H_INCLUDED

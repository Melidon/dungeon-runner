#ifndef RENDER_BACKGROUND_H_INCLUDED
#define RENDER_BACKGROUND_H_INCLUDED

#include "data.h"

void draw_background(GameState *gamestate, Graphics *graphics);

#endif // RENDER_BACKGROUND_H_INCLUDED

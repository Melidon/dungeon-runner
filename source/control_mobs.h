#ifndef CONTROL_MOBS_H_INCLUDED
#define CONTROL_MOBS_H_INCLUDED

#include "data.h"

void control_mobs(Character *mob, const Character *player);

#endif // CONTROL_MOBS_H_INCLUDED

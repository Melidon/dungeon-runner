#ifndef RENDER_CHARACTER_H_INCLUDED
#define RENDER_CHARACTER_H_INCLUDED

#include "data.h"

void draw_character(Character *character, Graphics *graphics, const GameData *gamedata);
void draw_player_stat(Character *player, const GameData *gamedata, Graphics *graphics);

#endif // RENDER_CHARACTER_H_INCLUDED

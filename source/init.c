#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "init.h"
#include "load_map.h"
#include "load_save.h"
#include "save.h"

#include "debugmalloc.h"

/* initialize sdl */
static void sdl_init(Graphics *graphics)
{
    graphics->screen_size = (IntVector2) {WIDTH, HEIGHT};
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        SDL_Log("Can't start SDL: %s", SDL_GetError());
        exit(1);
    }
    graphics->window = SDL_CreateWindow("Dungeon Runner", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, graphics->screen_size.x, graphics->screen_size.y, 0);
    if(graphics->window == NULL)
    {
        SDL_Log("Can't create window: %s", SDL_GetError());
        exit(1);
    }
    graphics->renderer = SDL_CreateRenderer(graphics->window, -1, SDL_RENDERER_SOFTWARE);
    if (graphics->renderer == NULL)
    {
        SDL_Log("Can't create renderer: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(graphics->renderer);
}

/* load the map_list */
static void load_map_list(GameData *gamedata)
{
    FILE *map_list = fopen("data/map_list.txt", "r");
    if(map_list != NULL)
    {
        fscanf(map_list, "%d\n", &gamedata->map_amount);
        gamedata->map_name_list = (char**) malloc(gamedata->map_amount * sizeof(char*));
        for(int i = 0; i < gamedata->map_amount; i++)
        {
            gamedata->map_name_list[i] = (char*) malloc(32 * sizeof(char));
            fscanf(map_list, "%[^\n]\n", gamedata->map_name_list[i]);
        }
        fclose(map_list);
    }
    else
    {
        perror("Can't open map_list");
        exit(1);
    }
}

/* load all weapons from the list */
static void load_weapons(GameData *gamedata)
{
    FILE *weapon_list = fopen("data/weapon_list.txt", "r");
    if(weapon_list != NULL)
    {
        fscanf(weapon_list, "%d", &gamedata->weapon_amount);
        gamedata->weapon_list = (Weapon*) malloc(gamedata->weapon_amount * sizeof(Weapon));
        for(int i = 0; i < gamedata->weapon_amount; i++)
        {
            fscanf(weapon_list, "%s %dd%d", gamedata->weapon_list[i].name, &gamedata->weapon_list[i].damage.amount, &gamedata->weapon_list[i].damage.sides);
        }
        fclose(weapon_list);
    }
    else
    {
        perror("Can't open weapon_list");
        /*gamedata->weapon_amount = 1;
        gamedata->weapon_list = (Weapon*) malloc(sizeof(Weapon));
        strcpy(gamedata->weapon_list[0].name, "Unknown weapon");
        gamedata->weapon_list[0].damage.amount = 0;
        gamedata->weapon_list[0].damage.sides = 0;*/
        exit(1);
    }
}

/* load all armors from the list */
static void load_armors(GameData *gamedata)
{
    FILE *armor_list = fopen("data/armor_list.txt", "r");
    if(armor_list != NULL)
    {
        fscanf(armor_list, "%d", &gamedata->armor_amount);
        gamedata->armor_list = (Armor*) malloc(gamedata->armor_amount * sizeof(Armor));
        for(int i = 0; i < gamedata->armor_amount; i++)
        {
            fscanf(armor_list, "%s %d", gamedata->armor_list[i].name, &gamedata->armor_list[i].defense_mod);
        }
        fclose(armor_list);
    }
    else
    {
        perror("Can't open armor_list");
        /*gamedata->armor_amount = 1;
        gamedata->armor_list = (Armor*) malloc(sizeof(Armor));
        strcpy(gamedata->armor_list[0].name, "Unknown armor");
        gamedata->armor_list[0].defense_mod = 0;*/
        exit(1);
    }
}

/* load all mobs from the mob_list */
static void load_mobs(GameData *gamedata)
{
    FILE *mobs = fopen("data/mobs.txt", "r");
    if(mobs != NULL)
    {
        fscanf(mobs, "%d\n", &gamedata->mob_amount);
        gamedata->mob_list = (Character*) malloc(gamedata->mob_amount * sizeof(Character));
        for(int i = 0; i < gamedata->mob_amount; i++)
        {
            fscanf(mobs, "name: %s health: %d, speed: %d, attack_mod: %d, damage_mod: %d, defense_mod: %d, weapon_id: %d, armor_id: %d, size: %d, link: %s\n",
                    gamedata->mob_list[i].name,
                    &gamedata->mob_list[i].max_health,
                    &gamedata->mob_list[i].speed,
                    &gamedata->mob_list[i].attack_mod,
                    &gamedata->mob_list[i].damage_mod,
                    &gamedata->mob_list[i].defense_mod,
                    &gamedata->mob_list[i].inventory.weapon_id,
                    &gamedata->mob_list[i].inventory.armor_id,
                    &gamedata->mob_list[i].animation_size,
                    gamedata->mob_list[i].link
                   );

            gamedata->mob_list[i].actual_health = gamedata->mob_list[i].max_health;
            gamedata->mob_list[i].inventory.heal_potion = 0;
            gamedata->mob_list[i].inventory.key_amount = 0;
            gamedata->mob_list[i].inventory.money.gold = 0;
            gamedata->mob_list[i].inventory.money.silver = 0;
            gamedata->mob_list[i].inventory.money.copper = 0;

            gamedata->mob_list[i].faceing_right = true;
            gamedata->mob_list[i].animation_counter = 0;
            gamedata->mob_list[i].animation_id = i+1;
            gamedata->mob_list[i].text_timer = 0;
        }
        fclose(mobs);
    }
    else
    {
        perror("Can't load mobs");
        exit(1);
    }
}

static void load_textures(Graphics *graphics, GameData *gamedata)
{
    graphics->textures.menu_background = IMG_LoadTexture(graphics->renderer, "images/menu_background.png");
    if(graphics->textures.menu_background == NULL)
    {
        SDL_Log("Error while loading menu_background.png: %s", IMG_GetError());
    }

    /* load the textures for the map */
    graphics->textures.tileset = IMG_LoadTexture(graphics->renderer, "images/tileset.png");
    if(graphics->textures.tileset == NULL)
    {
        SDL_Log("Error while loading tileset.png: %s", IMG_GetError());
        exit(1);
    }

    /* load player animation */
    graphics->textures.animations = (SDL_Texture**) malloc((gamedata->mob_amount+1) * sizeof(SDL_Texture*));
    graphics->textures.animations[0] = IMG_LoadTexture(graphics->renderer, "images/characters/player_animation.png");
    if(graphics->textures.animations[0] == NULL)
    {
        SDL_Log("Error while loading player_animation.png: %s", IMG_GetError());
        exit(1);
    }

    /* load animations for mobs */
    for(int i = 0; i < gamedata->mob_amount; i++)
    {
        graphics->textures.animations[i+1] = IMG_LoadTexture(graphics->renderer, gamedata->mob_list[i].link);
        if(graphics->textures.animations[i+1] == NULL)
        {
            SDL_Log("Error while loading mob animation[%d]: %s", i, IMG_GetError());
            exit(1);
        }
    }
    graphics->textures.gold = IMG_LoadTexture(graphics->renderer, "images/icons/gold.png");
    if(graphics->textures.gold == NULL)
    {
        SDL_Log("Error while loading gold icon: %s", IMG_GetError());
    }
    graphics->textures.silver = IMG_LoadTexture(graphics->renderer, "images/icons/silver.png");
    if(graphics->textures.silver == NULL)
    {
        SDL_Log("Error while loading silver icon: %s", IMG_GetError());
    }
    graphics->textures.copper = IMG_LoadTexture(graphics->renderer, "images/icons/copper.png");
    if(graphics->textures.copper == NULL)
    {
        SDL_Log("Error while loading copper icon: %s", IMG_GetError());
    }
    graphics->textures.shield = IMG_LoadTexture(graphics->renderer, "images/icons/shield.png");
    if(graphics->textures.shield == NULL)
    {
        SDL_Log("Error while loading shield.png: %s", IMG_GetError());
    }
    graphics->textures.heart = IMG_LoadTexture(graphics->renderer, "images/icons/heart.png");
    if(graphics->textures.heart == NULL)
    {
        SDL_Log("Error while loading heart.png: %s", IMG_GetError());
    }
}

/* the timer */
static Uint32 timer(Uint32 ms, void *param) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return ms;   /* next wait time */
}

static void load_font(Graphics *graphics)
{
    TTF_Init();
    graphics->font = TTF_OpenFont("LiberationSerif-Regular.ttf", 16);
    if(!graphics->font)
    {
        SDL_Log("Can't open font! %s\n", TTF_GetError());
        exit(1);
    }
}

/* TODO: REMOVE */
/*static void show_loaded_mobs(GameData *gamedata)
{
    for(int i = 0; i < gamedata->mob_amount; i++)
    {
        printf("%s\n", gamedata->mob_list[i].name);
        printf("    link: %s\n", gamedata->mob_list[i].link);
        printf("    health: %d\n", gamedata->mob_list[i].max_health);
        printf("    speed: %d\n", gamedata->mob_list[i].speed);
        printf("    attack_mod: %d\n", gamedata->mob_list[i].attack_mod);
        printf("    damage_mod: %d\n", gamedata->mob_list[i].damage_mod);
        printf("    defense_mod: %d\n", gamedata->mob_list[i].defense_mod);
        printf("    weapon_id: %d\n", gamedata->mob_list[i].inventory.weapon_id);
        printf("        name: %s\n", gamedata->weapon_list[gamedata->mob_list[i].inventory.weapon_id].name);
        printf("        amount: %d\n", gamedata->weapon_list[gamedata->mob_list[i].inventory.weapon_id].damage.amount);
        printf("        sides: %d\n", gamedata->weapon_list[gamedata->mob_list[i].inventory.weapon_id].damage.sides);
        printf("    armor_id: %d\n", gamedata->mob_list[i].inventory.armor_id);
        printf("        name: %s\n", gamedata->armor_list[gamedata->mob_list[i].inventory.armor_id].name);
        printf("        defense_mod: %d\n", gamedata->armor_list[gamedata->mob_list[i].inventory.armor_id].defense_mod);
        printf("    size: %d\n", gamedata->mob_list[i].animation_size);
    }
}*/

void init(GameState *gamestate, GameData *gamedata, Graphics *graphics)
{
    /* sdl init */
    sdl_init(graphics);

    /* load gamedata */
    load_map_list(gamedata);
    load_weapons(gamedata);
    load_armors(gamedata);
    load_mobs(gamedata);

    /* TODO: REMOVE */
    //show_loaded_mobs(gamedata);

    /* load gamestate */
    load_save(gamestate);
    gamestate->current_enemy = NULL;
    gamestate->current_map = NULL;
    load_map(gamestate, gamedata);

    /* load textures */
    load_textures(graphics, gamedata);

    /* create timer */
    graphics->id = SDL_AddTimer(1000 / FPS, timer, NULL);

    /* set the base state */
    gamestate->state = MAIN_MENU;

    /* load font */
    load_font(graphics);
}

#include <stdio.h>
#include <string.h>

#include "draw.h"

#include "render_character.h"
#include "render.h"

void draw_character(Character *character, Graphics *graphics, const GameData *gamedata)
{
    /* reset the animation, if the character just started an action*/
    if(character->animation_counter == 50)
    {
        character->frame = 0;
        character->delay = 0;
    }

    /* freeze the animation, if the character is dead */
    if(character->animation_type == DEATH && character->animation_counter == 1)
    {
        character->frame = 9;
        character->delay = 0;
    }

    /* move to the next frame */
    character->delay++;
    character->delay%=5;
    if(character->delay == 0)
    {
        character->frame++;
        character->frame%=10;
    }

    /* flip if necessary */
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    if(!character->faceing_right)
    {
        flip = SDL_FLIP_HORIZONTAL;
    }

    /* 1st, draw shadow for the caracter */
    if(character->animation_size > 32)
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find the shadow on the tileset */
                   112, 512, 32, 16,
                   /* find the position to draw it */
                   (character->position.x - graphics->camera_postion.x)*FIELDSIZE, (character->position.y+0.71875 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE/2,
                   0, NULL, flip);
    }
    else
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find the shadow on the tileset */
                   0, 512, 16, 16,
                   /* find the position to draw it */
                   (character->position.x+0.25 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y+0.71875 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE/2, FIELDSIZE/2,
                   0, NULL, flip);
    }

    /* 2nd, draw the caracter */
    draw_image(graphics->renderer, graphics->textures.animations[character->animation_id],
               /* find the right frame, on the image */
               character->frame * character->animation_size, character->animation_type * character->animation_size, character->animation_size, character->animation_size,
               /* find the character's position */
               (character->position.x - graphics->camera_postion.x - (character->animation_size - 32) / 32.0 / 2.0) * FIELDSIZE, (character->position.y - graphics->camera_postion.y - (character->animation_size - 32) / 32.0 ) * FIELDSIZE, FIELDSIZE * character->animation_size / 32.0, FIELDSIZE * character->animation_size / 32.0,
               0, NULL, flip);

    /*3rd, draw the characters health */
    if(character->actual_health > 0)
    {
        char text[16];
        sprintf(text, "%4d    %4d", character->actual_health, 10 + character->defense_mod + gamedata->armor_list[character->inventory.armor_id].defense_mod);
        draw_text(graphics, (character->position.x + 0.375 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y - 0.1 - graphics->camera_postion.y)*FIELDSIZE, text, 0, 0, 0, FIELDSIZE/64, true);
        //shield icon
        draw_image(graphics->renderer, graphics->textures.shield,
               0, 0, 32, 32,
               (character->position.x + 0.875 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y - 0.225 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE/4, FIELDSIZE/4,
               0, NULL, SDL_FLIP_NONE);
        //heart icon
        draw_image(graphics->renderer, graphics->textures.heart,
               0, 0, 48, 48,
               (character->position.x + 0.25 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y - 0.225 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE/4, FIELDSIZE/4,
               0, NULL, SDL_FLIP_NONE);
    }
    else
    {
        draw_text(graphics, (character->position.x + 0.5 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y + 0.6 - graphics->camera_postion.y)*FIELDSIZE, "Dead", 255, 0, 0, FIELDSIZE/64, true);
    }

    /* 4th, draw other texts */
    if(character->text_timer > 0)
    {
        draw_text(graphics, (character->position.x + 0.5 - graphics->camera_postion.x)*FIELDSIZE, (character->position.y + 0.35 - graphics->camera_postion.y)*FIELDSIZE, character->text, 255, 127, 0, FIELDSIZE/64, true);
        character->text_timer--;
    }
}

void draw_player_stat(Character *player, const GameData *gamedata, Graphics *graphics)
{
    char text[64];
    /* money */
    /* gold */
    sprintf(text, "%4d", player->inventory.money.gold);
    draw_text(graphics, 0, 0, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.gold,
               0, 0, 512, 512,
               44, 0, 32, 32,
               0, NULL, SDL_FLIP_NONE);
    /* silver */
    sprintf(text, "%4d", player->inventory.money.silver);
    draw_text(graphics, 64, 0, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.silver,
               0, 0, 512, 512,
               108, 0, 32, 32,
               0, NULL, SDL_FLIP_NONE);
    /* copper */
    sprintf(text, "%4d", player->inventory.money.copper);
    draw_text(graphics, 128, 0, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.copper,
               0, 0, 512, 512,
               172, 0, 32, 32,
               0, NULL, SDL_FLIP_NONE);

    /* weapon */
    sprintf(text, "%s, %dd%d+%d (+%d)", gamedata->weapon_list[player->inventory.weapon_id].name, gamedata->weapon_list[player->inventory.weapon_id].damage.amount, gamedata->weapon_list[player->inventory.weapon_id].damage.sides, player->damage_mod, player->attack_mod);
    draw_text(graphics, 64, 48, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find a sword on the tileset */
               128, 384, 16, 16,
               /* place it ti the right position */
               -16, 24, 64, 64,
               120, NULL, SDL_FLIP_VERTICAL);

    /* heal potion */
    sprintf(text, "%d", player->inventory.heal_potion);
    draw_text(graphics, 32, 96, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the heal potion on the tileset */
               0, 384, 16, 16,
               /* place it to the right position */
               0-16, 96-32, 64, 64,
               0, NULL, SDL_FLIP_NONE);

    /* keys */
    strcpy(text, " ");
    for(int i = 0; i < player->inventory.key_amount; i++)
    {
        char helper[8];
        sprintf(helper, "%d, ", player->inventory.keys[i]);
        strcat(text, helper);
    }
    draw_text(graphics, 32, 128, text, 0, 0, 0, 2, false);
    draw_image(graphics->renderer, graphics->textures.tileset,
               160, 352, 16, 16,
               0-16+4, 128-32-8, 64, 64,
               0, NULL, SDL_FLIP_NONE);
}

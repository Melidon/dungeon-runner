#ifndef CONTROL_PLAYER_H_INCLUDED
#define CONTROL_PLAYER_H_INCLUDED

#include "data.h"

void control_player(const UserInput *userinput, Character *player);

#endif // CONTROL_PLAYER_H_INCLUDED

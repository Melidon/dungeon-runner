#ifndef ENDGAME_H_INCLUDED
#define ENDGAME_H_INCLUDED

void endgame(GameState *gamestate, GameData *gamedata, Graphics *graphics);

#endif // ENDGAME_H_INCLUDED

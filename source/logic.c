#include "logic.h"
#include "load_map.h"
#include "load_save.h"
#include "control_mobs.h"
#include "my_math.h"
#include "move.h"
#include "calculate_damage.h"
#include "control_player.h"
#include "interact.h"

static void main_menu(const UserInput *userinput, GameState *gamestate)
{
    if(userinput->heal)
    {
        gamestate->state = GAME;
    }
}

static void game_logic(const UserInput *userinput, const GameData *gamedata, GameState *gamestate)
{
    /* restart the map if you are dead */
    if(gamestate->player.actual_health <= 0 && gamestate->player.animation_counter == 1)
    {
        load_save(gamestate);
        load_map(gamestate, gamedata);
    }

    /* control the player's animation type */
    control_player(userinput, &gamestate->player);

    /* control the mobs' animation type*/
    for(int i = 0; i < gamestate->current_enemy_amount; i++)
    {
        control_mobs(&gamestate->current_enemy[i], &gamestate->player);
    }

    /* move all characters */
    move(&gamestate->player, gamestate);
    for(int i = 0; i < gamestate->current_enemy_amount; i++)
    {
        move(&gamestate->current_enemy[i], gamestate);
    }

    /* calculate all damage */
    for(int i = 0; i < gamestate->current_enemy_amount; i++)
    {
        calculate_damage(&gamestate->current_enemy[i], &gamestate->player, gamedata);
    }

    /* open doors, loot chests, etc... */
    interact(gamestate, gamedata);
}

void logic(const UserInput *userinput, const GameData *gamedata, GameState *gamestate)
{
    gamestate->frame_enabled = userinput->frame_enabled;
    if(gamestate->frame_enabled)
    {
        switch(gamestate->state)
        {
            case MAIN_MENU:
                main_menu(userinput, gamestate);
                break;
            case GAME:
                game_logic(userinput, gamedata, gamestate);
                break;
            default:
                break;
        }
    }
}

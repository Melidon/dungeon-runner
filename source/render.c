#include <stdlib.h>
#include <stdio.h>

#include "draw.h"

#include "render_background.h"
#include "render_character.h"

#include "render.h"

static void draw_main_menu(Graphics *graphics)
{
    /* draw the background */
    draw_image(graphics->renderer, graphics->textures.menu_background,
               0, 0, 540, 304,
               0, 0, WIDTH, HEIGHT,
               0, NULL, SDL_FLIP_NONE);
    /* draw the title and the hint */
    draw_text(graphics, WIDTH/2, HEIGHT*1/4, "DUNGEON RUNNER", 159, 63, 0, 8, true);
    draw_text(graphics, WIDTH/2, HEIGHT*15/16, "Press \"G\" to start!", 223, 223, 223, 8, true);
}

static void move_camera(const GameState *gamestate, Graphics *graphics)
{
    graphics->camera_postion = (DoubleVector2) {gamestate->player.position.x - WIDTH/FIELDSIZE/2 + 0.5, gamestate->player.position.y - HEIGHT/FIELDSIZE/2 + 0.5};
    if(graphics->camera_postion.x > gamestate->current_map_size.x - WIDTH/FIELDSIZE)
    {
        graphics->camera_postion.x = gamestate->current_map_size.x - WIDTH/FIELDSIZE;
    }
    if(graphics->camera_postion.y > gamestate->current_map_size.y - HEIGHT/FIELDSIZE)
    {
        graphics->camera_postion.y = gamestate->current_map_size.y - HEIGHT/FIELDSIZE;
    }
    if(graphics->camera_postion.x < 0)
    {
        graphics->camera_postion.x = 0;
    }
    if(graphics->camera_postion.y < 0)
    {
        graphics->camera_postion.y = 0;
    }
}

static void draw_game(const GameData *gamedata, GameState *gamestate, Graphics *graphics)
{
    /* control the camera */
    move_camera(gamestate, graphics);

    /* draw the map here */
    draw_background(gamestate, graphics);

    /* drawing the player, and all mobs */
    for(int i = 0; i < gamestate->current_enemy_amount; i++)
    {
        draw_character(&gamestate->current_enemy[i], graphics, gamedata);
    }
    draw_character(&gamestate->player, graphics, gamedata);

    /* draw the player's statistics */
    draw_player_stat(&gamestate->player, gamedata, graphics);
}

void render(const GameData *gamedata, GameState *gamestate, Graphics *graphics)
{
    if(gamestate->frame_enabled)
    {
        /* clear renderer, before doing anything */
        SDL_RenderClear(graphics->renderer);

        /* draw the appropriate scene */
        switch(gamestate->state)
        {
            case MAIN_MENU:
                draw_main_menu(graphics);
                break;
            case GAME:
                draw_game(gamedata, gamestate, graphics);
                break;
            default:
                break;
        }

        /* show renderer on screen */
        SDL_RenderPresent(graphics->renderer);
    }
}

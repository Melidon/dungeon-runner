#include "draw.h"

#include "render_background.h"

static void draw_floor(const Field *floor, const IntVector2 position, Graphics *graphics)
{
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the floor on the tileset */
               floor->id%30 * 16, 112 + floor->id/30 * 16, 16, 16,
               /* find the position to draw it */
               (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
               0, NULL, SDL_FLIP_NONE);
}

static void draw_wall(const Field *wall, const IntVector2 position, Graphics *graphics)
{
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the wall on the tileset */
               144, 144, 16, 16,
               /* find the position to draw it */
               (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
               0, NULL, SDL_FLIP_NONE);
}

/* TODO: make it work well for the rotated doors */
static void draw_door(const Field *door, const IntVector2 position, Graphics *graphics, const GameState *gamestate)
{
    /* rotate if necessary */
    double angle = 0;
    if(gamestate->current_map[position.y-1][position.x].fieldtype != FLOOR)
    {
        angle = 90;
    }

    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the floor on the tileset */
               368, 112, 16, 16,
               /* find the position to draw it */
               (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
               /* apply rotation */
               angle, NULL, SDL_FLIP_NONE);

    if(door->walkable)
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find an opened on the tileset */
                   144, 176, 16, 48,
                   /* find the position to draw it */
                   (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y-0.5 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE*1.5,
                   /* apply rotation */
                   angle, NULL, SDL_FLIP_NONE);
    }
    else
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find a closed door on the tileset */
                   32, 176, 16, 32,
                   /* find the position to draw it */
                   (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
                   /* apply rotation */
                   angle, NULL, SDL_FLIP_NONE);
    }
}

static void draw_chest(const Field *chest, const IntVector2 position, Graphics *graphics)
{
    //TODO: MAKE FLOOR TO FIT THE WALLS AUTOMATICALLY

    /* 1st, draw the floor under the chest */
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the floor on the tileset */
               0, 112, 16, 16,
               /* find the position to draw it */
               (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
               0, NULL, SDL_FLIP_NONE);

    /* 2nd, draw the shadow of the chest */
    draw_image(graphics->renderer, graphics->textures.tileset,
               /* find the shadow on the tileset */
               16, 512, 16, 16,
               /* find the position to draw it */
               (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y+0.125 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
               0, NULL, SDL_FLIP_NONE);

    /* 3rd, finally draw the chest */
    if(chest->special.chest->open)
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find an open chest on the tileset */
                   48, 304, 16, 16,
                   /* find the position to draw it */
                   (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y-0.125 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
                   0, NULL, SDL_FLIP_NONE);
    }
    else
    {
        draw_image(graphics->renderer, graphics->textures.tileset,
                   /* find a closed chest on the tileset */
                   32, 304, 16, 16,
                   /* find the position to draw it */
                   (position.x - graphics->camera_postion.x)*FIELDSIZE, (position.y-0.125 - graphics->camera_postion.y)*FIELDSIZE, FIELDSIZE, FIELDSIZE,
                   0, NULL, SDL_FLIP_NONE);
    }
}

void draw_background(GameState *gamestate, Graphics *graphics)
{
    for(int i = 0; i < gamestate->current_map_size.y; i++)
    {
        for(int j = 0; j < gamestate->current_map_size.x; j++)
        {
            switch(gamestate->current_map[i][j].fieldtype)
            {
                case FLOOR:
                    draw_floor(&gamestate->current_map[i][j], (IntVector2) {j, i}, graphics);
                    break;
                case WALL:
                    draw_wall(&gamestate->current_map[i][j], (IntVector2) {j, i}, graphics);
                    break;
                case DOOR:
                    draw_door(&gamestate->current_map[i][j], (IntVector2) {j, i}, graphics, gamestate);
                    break;
                case CHEST:
                    draw_chest(&gamestate->current_map[i][j], (IntVector2) {j, i}, graphics);
                    break;
                default:
                    break;
            }
            if(gamestate->current_map[i][j].text_timer > 0)
            {
                draw_text(graphics, (j+0.5 - graphics->camera_postion.x)*FIELDSIZE, (i+0.25 - graphics->camera_postion.y)*FIELDSIZE, gamestate->current_map[i][j].text, 255, 127, 0, FIELDSIZE/64, true);
                gamestate->current_map[i][j].text_timer--;
            }
        }
    }
}

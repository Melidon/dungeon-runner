#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include "data.h"

void draw_image(SDL_Renderer *renderer, SDL_Texture *image,
                const int src_x, const int src_y, const int src_w, const int src_h,
                const int dst_x, const int dst_y, const int dst_w, const int dst_h,
                const double angle, const SDL_Point *center, const SDL_RendererFlip flip);

void draw_text(Graphics *graphics, double x, double y, char *text, int r, int g, int b, double size, bool to_the_middle);

#endif // DRAW_H_INCLUDED

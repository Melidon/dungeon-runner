#ifndef SAVE_H_INCLUDED
#define SAVE_H_INCLUDED

#include "data.h"

void save_the_game(const GameState *gamestate);

#endif // SAVE_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include "my_math.h"

#include "calculate_damage.h"

/* calculate the damage */
static void deal_damage(Character *dealer, Character *receiveer, const GameData *gamedata)
{
    /* calculate */
    int damage = roll(gamedata->weapon_list[dealer->inventory.weapon_id].damage) + dealer->damage_mod;
    if(damage < 0)
    {
        damage = 0;
    }

    /* graphics */
    if(receiveer->actual_health > 0)
    {
        receiveer->text_timer = 25;
        sprintf(receiveer->text, "%d", -damage);
    }

    /* calculate new health */
    receiveer->actual_health -= damage;
}


static void attack(Character *dealer, Character *receiveer, const GameData *gamedata)
{
    /* calculate the dealers attack mod + 1d20, if it is higher than the receiveers defense, than deal damage */
    int attack_mod = dealer->attack_mod + roll((Dice) {1, 20});
    int defense_mod = 10 + receiveer->defense_mod + gamedata->armor_list[receiveer->inventory.armor_id].defense_mod;
    if(attack_mod >= defense_mod)
    {
        deal_damage(dealer, receiveer, gamedata);
    }
    else if(receiveer->actual_health > 0)
    {
        receiveer->text_timer = 25;
        sprintf(receiveer->text, "MISS");
    }
}

void calculate_damage(Character *mob, Character *player, const GameData *gamedata)
{
    const double attack_range = 0.75;

    /* check if the player is attacking, if this is the case, than calculate damage */
    if(distance(player->position, mob->position) <= attack_range && player->animation_counter == 15 && player->animation_type == ATTACK)
    {
        attack(player, mob, gamedata);
    }

    /* check if the mob is attacking, if this is the case, than calculate damage */
    if(distance(player->position, mob->position) <= attack_range && mob->animation_counter == 15 && mob->animation_type == ATTACK)
    {
        attack(mob, player, gamedata);
    }
}

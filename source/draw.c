#include "draw.h"

void draw_image(SDL_Renderer *renderer, SDL_Texture *image,
                const int src_x, const int src_y, const int src_w, const int src_h,
                const int dst_x, const int dst_y, const int dst_w, const int dst_h,
                const double angle, const SDL_Point *center, const SDL_RendererFlip flip)
{
    /* find the position on the image */
    SDL_Rect src = {src_x, src_y, src_w, src_h};
    /* find the position on the renderer */
    SDL_Rect dst = {dst_x, dst_y, dst_w, dst_h};
    /* just copy-paste */
    SDL_RenderCopyEx(renderer, image, &src, &dst, angle, center, flip);
}

void draw_text(Graphics *graphics, double x, double y, char *text, int r, int g, int b, double size, bool to_the_middle)
{
    SDL_Surface *surface;
    SDL_Texture *texture;
    surface = TTF_RenderUTF8_Solid(graphics->font, text, (SDL_Color) {r, g, b});
    texture = SDL_CreateTextureFromSurface(graphics->renderer, surface);
    SDL_Rect dst;
    if(to_the_middle)
    {
        dst = (SDL_Rect) {x - surface->w*size/2, y - surface->h*size/2, surface->w*size, surface->h*size};
    }
    else
    {
        dst = (SDL_Rect) {x, y, surface->w*size, surface->h*size};
    }
    SDL_RenderCopy(graphics->renderer, texture, NULL, &dst);
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}

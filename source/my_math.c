#include <stdlib.h>

#include "my_math.h"

double distance(DoubleVector2 a, DoubleVector2 b)
{
    return sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}

int roll(Dice dice)
{
    int sum = 0;
    for(int i = 0; i < dice.amount; i++)
    {
        sum += rand()%dice.sides + +1;
    }
    return sum;
}

int sgn(double n)
{
    if(n == 0)
    {
        return 0;
    }
    else if(n > 0)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

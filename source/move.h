#ifndef MOVE_H_INCLUDED
#define MOVE_H_INCLUDED

#include "data.h"

void move(Character* character, GameState *gamestate);

#endif // MOVE_H_INCLUDED

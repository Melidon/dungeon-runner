#include "control_mobs.h"

#include "my_math.h"

void control_mobs(Character *mob, const Character *player)
{
    const double attack_range = 0.75;
    const double view_range  = 3.0;

    if(mob->actual_health > 0)
    {
        if(mob->animation_counter == 0)
        {
            if(player->actual_health > 0)
            {
                if(distance(player->position, mob->position) <= attack_range)
                {
                    mob->animation_type = ATTACK;
                    mob->animation_counter = 50;
                }
                else if(distance(player->position, mob->position) <= view_range)
                {
                    mob->animation_type = WALK;
                    DoubleVector2 movement;
                    if(fabs(player->position.x - mob->position.x) > 0.03)
                    {
                        movement.x = sgn(player->position.x - mob->position.x);
                    }
                    else
                    {
                        movement.x = 0;
                    }
                    if(fabs(player->position.y - mob->position.y) > 0.03)
                    {
                        movement.y = sgn(player->position.y - mob->position.y);
                    }
                    else
                    {
                        movement.y = 0;
                    }
                    if(movement.x*movement.x + movement.y*movement.y > 1*1)
                    {
                        movement.x /= sqrt(2);
                        movement.y /= sqrt(2);
                    }
                    mob->movement = movement;
                }
                else
                {
                    mob->animation_type = IDLE;
                }
            }
            else
            {
                mob->animation_type = GESTURE;
            }
        }
        else
        {
            mob->animation_counter--;
        }
    }
    else //if health <= 0
    {
        mob->animation_type = DEATH;
        if(mob->animation_counter == 0)
        {
            mob->animation_counter = 50;
        }
        /* it is useful, because, the DEATH animation won't repeat */
        else if(mob->animation_counter > 1)
        {
            mob->animation_counter--;
        }
    }
}

#include <stdlib.h>
#include <time.h>

#include "data.h"
#include "init.h"
#include "handle_user_input.h"
#include "logic.h"
#include "render.h"
#include "endgame.h"

int main(int argc, char *argv[]) {
    /* shuffle the random generator */
    srand(time(NULL));

    /* create essentials*/
    GameState gamestate;
    GameData gamedata;
    Graphics graphics;
    UserInput userinput;

    /* initialize everything */
    init(&gamestate, &gamedata, &graphics);
    userinput = (UserInput) {false, false, false, false, false, false, false, true, false};

    /* TODO: remove */
    //gamestate.player.animation_id = 1;

    /* the game loop */
    while(userinput.game_is_running)
    {
        handle_user_input(&userinput);
        logic(&userinput, &gamedata, &gamestate);
        render(&gamedata, &gamestate, &graphics);
    }

    /* free */
    endgame(&gamestate, &gamedata, &graphics);
    return 0;
}

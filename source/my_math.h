#ifndef MY_MATH_H_INCLUDED
#define MY_MATH_H_INCLUDED

#include <math.h>
#include "data.h"

double distance(DoubleVector2 a, DoubleVector2 b);
int roll(Dice dice);
int sgn(double n);

#endif // MY_MATH_H_INCLUDED

#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "stdbool.h"

enum {WIDTH = 1536, HEIGHT = 864};
enum {FIELDSIZE = 128};
enum {FPS = 50};

typedef enum
{
    IDLE, GESTURE, WALK, ATTACK, DEATH
} AnimationType;

typedef struct
{
    int amount, sides;
} Dice;

typedef struct
{
    int x, y;
} IntVector2;

typedef struct
{
    double x, y;
} DoubleVector2;

typedef struct
{
    int gold, silver, copper;
} Money;

typedef struct
{
    char name[32];
    Dice damage;
} Weapon;

typedef struct
{
    char name[32];
    int defense_mod;
} Armor;

typedef struct
{
    int weapon_id;
    int armor_id;
    int heal_potion;
    Money money;
    int key_amount;
    int keys[32];
} Inventory;

typedef struct
{
    /* important to save */
    char name[16];
    int speed;
    int max_health;
    int actual_health;
    int attack_mod;
    int damage_mod;
    int defense_mod;
    Inventory inventory;

    /* pointless to save */

    bool faceing_right;
    DoubleVector2 position;
    DoubleVector2 movement;
    AnimationType animation_type;
    int animation_counter;
    int frame;
    int delay;
    int animation_size;
    int animation_id;
    char link[64];
    int text_timer;
    char text[16];
} Character;

typedef struct
{
    bool open;
    Inventory inventory;
} Chest;

typedef enum
{
    FLOOR, WALL, DOOR, CHEST
} FieldType;


typedef struct
{
    Chest *chest;
} SpecialField;

typedef struct
{
    FieldType fieldtype;
    int id;
    int walkable;
    int text_timer;
    char text[16];
    SpecialField special;
} Field;

typedef enum
{
    MAIN_MENU, GAME
} State;

/* for the acutal atate of the game */
typedef struct
{
    Character player;
    int current_map_id;
    IntVector2 current_map_size;
    Field** current_map;
    int current_enemy_amount;
    Character* current_enemy;
    /*--------*/
    bool frame_enabled;
    State state;
} GameState;

/* for constants */
typedef struct
{
    int map_amount;
    char** map_name_list;
    int weapon_amount;
    Weapon *weapon_list;
    int armor_amount;
    Armor *armor_list;
    int mob_amount;
    Character *mob_list;
} GameData;

typedef struct
{
    SDL_Texture *tileset;
    SDL_Texture **animations;
    SDL_Texture *menu_background;
    SDL_Texture *gold;
    SDL_Texture *silver;
    SDL_Texture *copper;
    SDL_Texture *shield;
    SDL_Texture *heart;
} Textures;

typedef struct
{
    DoubleVector2 camera_postion;

    IntVector2 screen_size;
    SDL_TimerID id;
    SDL_Window *window;
    SDL_Renderer *renderer;
    Textures textures;
    TTF_Font *font;
} Graphics;

typedef struct
{
    bool w, a, s, d, attack, heal, interact, game_is_running, frame_enabled;
} UserInput;

#endif // DATA_H_INCLUDED

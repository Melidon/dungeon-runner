#include <stdio.h>
#include <string.h>

#include "save.h"
#include "load_save.h"

#include "debugmalloc.h"

/* load save */
void load_save(GameState *gamestate)
{
    FILE *save = fopen("data/save.txt", "r");
    if(save != NULL)
    {
        /* loading save */
        fscanf(save, "name: %s\n", gamestate->player.name);
        fscanf(save, "health: %d\n", &gamestate->player.max_health);
        gamestate->player.actual_health = gamestate->player.max_health;
        fscanf(save, "speed: %d\n", &gamestate->player.speed);
        fscanf(save, "attack_mod: %d\n", &gamestate->player.attack_mod);
        fscanf(save, "damage_mod: %d\n", &gamestate->player.damage_mod);
        fscanf(save, "defense_mod: %d\n", &gamestate->player.defense_mod);
        fscanf(save, "weapon_id: %d\n", &gamestate->player.inventory.weapon_id);
        fscanf(save, "armor_id: %d\n", &gamestate->player.inventory.armor_id);
        fscanf(save, "heal_potion: %d\n", &gamestate->player.inventory.heal_potion);
        fscanf(save, "money: %d gold, %d silver, %d copper\n", &gamestate->player.inventory.money.gold, &gamestate->player.inventory.money.silver, &gamestate->player.inventory.money.copper);
        fscanf(save, "key_amount: %d\n", &gamestate->player.inventory.key_amount);
        for(int i = 0; i < gamestate->player.inventory.key_amount; i++)
        {
            fscanf(save, "%d", &gamestate->player.inventory.keys[i]);
        }
        fscanf(save, "\n");
        fscanf(save, "map_id: %d", &gamestate->current_map_id);
        fclose(save);
    }
    else
    {
        /* creating new character */
        strcpy(gamestate->player.name, "Joe");
        gamestate->player.max_health = 12;
        gamestate->player.actual_health = gamestate->player.max_health;
        gamestate->player.speed = 20;
        gamestate->player.attack_mod = 5;
        gamestate->player.damage_mod = 2;
        gamestate->player.defense_mod = 2;
        gamestate->player.inventory.weapon_id = 6;
        gamestate->player.inventory.armor_id = 3;
        gamestate->player.inventory.heal_potion = 1;
        gamestate->player.inventory.money.copper = 0;
        gamestate->player.inventory.money.silver = 0;
        gamestate->player.inventory.money.gold = 0;
        gamestate->player.inventory.key_amount = 0;
        gamestate->current_map_id = 0;
        perror("Can't open save");
        printf("Creating new save...\n");
        save_the_game(gamestate);
    }
    /*  */
    gamestate->player.faceing_right = true;
    gamestate->player.animation_counter = 0;
    gamestate->player.animation_size = 32;
    gamestate->player.text_timer = 0;
    gamestate->player.animation_id = 0;
    strcpy(gamestate->player.link, "images/characters/player_animation.png"); //It is actually useless
}

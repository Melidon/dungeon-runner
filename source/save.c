#include <stdio.h>

#include "save.h"

/* save the game to save.txt */
void save_the_game(const GameState *gamestate)
{
    FILE *save = fopen("data/save.txt", "w");
    if(save != NULL)
    {
        /* saveing the game */
        fprintf(save, "name: %s\n", gamestate->player.name);
        fprintf(save, "health: %d\n", gamestate->player.max_health);
        fprintf(save, "speed: %d\n", gamestate->player.speed);
        fprintf(save, "attack_mod: %d\n", gamestate->player.attack_mod);
        fprintf(save, "damage_mod: %d\n", gamestate->player.damage_mod);
        fprintf(save, "defense_mod: %d\n", gamestate->player.defense_mod);
        fprintf(save, "weapon_id: %d\n", gamestate->player.inventory.weapon_id);
        fprintf(save, "armor_id: %d\n", gamestate->player.inventory.armor_id);
        fprintf(save, "heal_potion: %d\n", gamestate->player.inventory.heal_potion);
        fprintf(save, "money: %d gold, %d silver, %d copper\n", gamestate->player.inventory.money.gold, gamestate->player.inventory.money.silver, gamestate->player.inventory.money.copper);
        fprintf(save, "key_amount: %d\n", gamestate->player.inventory.key_amount);
        for(int i = 0; i < gamestate->player.inventory.key_amount; i++)
        {
            fprintf(save, "%d ", gamestate->player.inventory.keys[i]);
        }
        fprintf(save, "\n");
        fprintf(save, "map_id: %d\n", gamestate->current_map_id);
        fclose(save);
    }
    else
    {
        perror("Can't save the game");
    }
}

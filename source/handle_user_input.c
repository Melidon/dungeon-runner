#include <stdbool.h>
#include <math.h>

#include "handle_user_input.h"

void handle_user_input(UserInput *user_input)
{
    SDL_Event event;
    SDL_WaitEvent(&event);

    if(event.type == SDL_USEREVENT && SDL_GetTicks() - event.user.timestamp < 1000 / FPS)
    {
        user_input->frame_enabled = true;
    }
    else
    {
        user_input->frame_enabled = false;
        switch(event.type)
        {
            case SDL_QUIT:
                user_input->game_is_running = false;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_w:
                        user_input->w = true;
                        break;
                    case SDLK_a:
                        user_input->a = true;
                        break;
                    case SDLK_s:
                        user_input->s = true;
                        break;
                    case SDLK_d:
                        user_input->d = true;
                        break;
                    case SDLK_f:
                        user_input->attack = true;
                        break;
                    case SDLK_g:
                        user_input->heal = true;
                        break;
                    case SDLK_h:
                        user_input->interact = true;
                        break;
                }
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_w:
                        user_input->w = false;
                        break;
                    case SDLK_a:
                        user_input->a = false;
                        break;
                    case SDLK_s:
                        user_input->s = false;
                        break;
                    case SDLK_d:
                        user_input->d = false;
                        break;
                    case SDLK_f:
                        user_input->attack = false;
                        break;
                    case SDLK_g:
                        user_input->heal = false;
                        break;
                    case SDLK_h:
                        user_input->interact = false;
                        break;
                }
                break;
        }
    }
}

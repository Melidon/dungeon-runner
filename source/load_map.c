#include <stdio.h>
#include <stdlib.h>

#include "load_map.h"

#include "debugmalloc.h"

static void apply_easter_eggs(GameState *gamestate)
{
    switch(gamestate->current_map_id)
    {
        /* my map */
        case 0:
            gamestate->current_map[1][8].walkable = true;
            gamestate->current_map[16][13].walkable = true;
            gamestate->current_map[22][13].walkable = true;
            break;
        /* Badi's map */
        case 1:
            break;
        default:
            break;
    }
}

void load_map(GameState *gamestate, const GameData *gamedata)
{
    FILE *map = fopen(gamedata->map_name_list[gamestate->current_map_id], "r");
    if(map != NULL)
    {
        if(gamestate->current_map != NULL)
        {
            for(int i = 0; i < gamestate->current_map_size.y; i++)
            {
                for(int j = 0; j < gamestate->current_map_size.x; j++)
                {
                    if(gamestate->current_map[i][j].fieldtype == CHEST)
                    {
                        free(gamestate->current_map[i][j].special.chest);
                    }
                }
                free(gamestate->current_map[i]);
            }
        }
        gamestate->current_enemy_amount = 0;
        fscanf(map, "%d %d\n", &gamestate->current_map_size.x, &gamestate->current_map_size.y);
        gamestate->current_map = (Field**) realloc(gamestate->current_map, gamestate->current_map_size.y * sizeof(Field*));
        for(int i = 0; i < gamestate->current_map_size.y; i++)
        {
            gamestate->current_map[i] = NULL;
            gamestate->current_map[i] = (Field*) malloc(gamestate->current_map_size.x * sizeof(Field));
        }
        char type;
        int id;
        /* loading map */
        for(int i = 0; i < gamestate->current_map_size.y; i++)
        {
            for(int j = 0; j < gamestate->current_map_size.x; j++)
            {
                fscanf(map, "%d%c", &id, &type);
                switch(type)
                {
                    case 'f': //floor
                        gamestate->current_map[i][j].fieldtype = FLOOR;
                        gamestate->current_map[i][j].id = id;
                        gamestate->current_map[i][j].walkable = true;
                        break;
                    case 'w': //wall
                        gamestate->current_map[i][j].fieldtype = WALL;
                        gamestate->current_map[i][j].id = id;
                        gamestate->current_map[i][j].walkable = false;
                        break;
                    case 'p': //player
                        gamestate->current_map[i][j].fieldtype = FLOOR;
                        gamestate->current_map[i][j].id = id;
                        gamestate->current_map[i][j].walkable = true;

                        gamestate->player.position.y = (double) i;
                        gamestate->player.position.x = (double) j;
                        break;
                    case 'd': //door
                        gamestate->current_map[i][j].fieldtype = DOOR;
                        gamestate->current_map[i][j].id = id;
                        gamestate->current_map[i][j].walkable = false;
                        break;
                    case 'c': //chest
                        gamestate->current_map[i][j].fieldtype = CHEST;
                        gamestate->current_map[i][j].id = id;
                        gamestate->current_map[i][j].walkable = false;
                        gamestate->current_map[i][j].special.chest = (Chest*) malloc(sizeof(Chest));
                        gamestate->current_map[i][j].special.chest->open = false;
                        /* load content later */
                        break;
                    case 'e': //enemy
                        gamestate->current_map[i][j].fieldtype = FLOOR;
                        gamestate->current_map[i][j].id = 64;
                        gamestate->current_map[i][j].walkable = true;

                        gamestate->current_enemy_amount++;
                        gamestate->current_enemy = (Character*) realloc(gamestate->current_enemy, gamestate->current_enemy_amount * sizeof(Character));
                        gamestate->current_enemy[gamestate->current_enemy_amount-1] = gamedata->mob_list[id]; //create a copy of a previously loaded mob
                        gamestate->current_enemy[gamestate->current_enemy_amount-1].position.y = (double) i;
                        gamestate->current_enemy[gamestate->current_enemy_amount-1].position.x = (double) j;
                        break;
                }
                gamestate->current_map[i][j].text_timer = 0;
            }
        }
        /* putting items to chests */
        fscanf(map, "\nchests:");
        for(int i = 0; i < gamestate->current_map_size.y; i++)
        {
            for(int j = 0; j < gamestate->current_map_size.x; j++)
            {
                if(gamestate->current_map[i][j].fieldtype == CHEST)
                {
                    fscanf(map, "\nweapon_id: %d, armor_id: %d, heal_potion: %d, gold: %d, silver: %d, copper: %d, key_amount: %d",
                           &gamestate->current_map[i][j].special.chest->inventory.weapon_id,
                           &gamestate->current_map[i][j].special.chest->inventory.armor_id,
                           &gamestate->current_map[i][j].special.chest->inventory.heal_potion,
                           &gamestate->current_map[i][j].special.chest->inventory.money.gold,
                           &gamestate->current_map[i][j].special.chest->inventory.money.silver,
                           &gamestate->current_map[i][j].special.chest->inventory.money.copper,
                           &gamestate->current_map[i][j].special.chest->inventory.key_amount
                           );
                    for(int k = 0; k < gamestate->current_map[i][j].special.chest->inventory.key_amount; k++)
                    {
                        fscanf(map, "%d", &gamestate->current_map[i][j].special.chest->inventory.keys[k]);
                    }
                }
            }
        }
        /* giving additional items to enemies */
        fscanf(map, "\nmobs:");
        for(int i = 0; i < gamestate->current_enemy_amount; i++)
        {
            fscanf(map, "\ngold: %d, silver: %d, copper: %d, key_amount: %d",
                   &gamestate->current_enemy[i].inventory.money.gold,
                   &gamestate->current_enemy[i].inventory.money.silver,
                   &gamestate->current_enemy[i].inventory.money.copper,
                   &gamestate->current_enemy[i].inventory.key_amount
                   );
            for(int k = 0; k < gamestate->current_enemy[i].inventory.key_amount; k++)
            {
                fscanf(map, "%d", &gamestate->current_enemy[i].inventory.keys[k]);
            }
        }
        fclose(map);

        /* just to make some fun */
        apply_easter_eggs(gamestate);
    }
    else
    {
        perror("Can't open map");
        exit(1);
    }
}

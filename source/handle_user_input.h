#ifndef HANDLE_USER_INPUT_H_INCLUDED
#define HANDLE_USER_INPUT_H_INCLUDED

#include "data.h"

void handle_user_input(UserInput *user_input);

#endif // HANDLE_USER_INPUT_H_INCLUDED

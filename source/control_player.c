#include <stdio.h>
#include <math.h>

#include "my_math.h"

#include "control_player.h"

static DoubleVector2 calculate_movement(const UserInput *user_input)
{
    DoubleVector2 movement = {0, 0};
    if(user_input->w) movement.y -= 1;
    if(user_input->a) movement.x -= 1;
    if(user_input->s) movement.y += 1;
    if(user_input->d) movement.x += 1;
    if(movement.x*movement.x + movement.y*movement.y > 1*1)
    {
        movement.y /= sqrt(2);
        movement.x /= sqrt(2);
    }
    return movement;
}

static void use_heal_potion(Character *player)
{
    if(player->inventory.heal_potion > 0)
    {
        if(player->actual_health >= player->max_health)
        {
            sprintf(player->text, "FULL HP!");
            player->text_timer = 50;
        }
        else
        {
            player->inventory.heal_potion--;
            int heal = roll((Dice) {1, 8});
            player->actual_health += heal;

            /* to not heal above max health */
            if(player->actual_health > player->max_health)
            {
                heal -= player->actual_health - player->max_health;
                player->actual_health = player->max_health;
            }

            sprintf(player->text, "+%d", heal);
            player->text_timer = 50;
        }
    }
    else
    {
        sprintf(player->text, "NO POTI!");
        player->text_timer = 20;
    }
}

void control_player(const UserInput *userinput, Character *player)
{
    if(player->actual_health > 0)
    {
        if(player->animation_counter == 0)
        {
            if(userinput->attack)
            {
                player->animation_type = ATTACK;
                player->animation_counter = 50;
            }
            else if(userinput->heal && player->text_timer == 0)
            {
                use_heal_potion(player);
            }
            else if(userinput->interact)
            {
                player->animation_type = GESTURE;
                player->animation_counter = 50;
            }
            else if(userinput->w ^ userinput->s || userinput->a ^ userinput->d)
            {
                player->animation_type = WALK;
                player->movement = calculate_movement(userinput);
            }
            else
            {
                player->animation_type = IDLE;
            }
        }
        else
        {
            player->animation_counter--;
        }
    }
    else //if health <= 0
    {
        player->animation_type = DEATH;
        if(player->animation_counter == 0)
        {
            player->animation_counter = 50;
        }
        else if(player->animation_counter > 1)
        {
            player->animation_counter--;
        }
    }
}

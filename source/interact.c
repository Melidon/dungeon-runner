#include <stdio.h>
#include <string.h>
#include "my_math.h"
#include "load_map.h"
#include "save.h"

#include "interact.h"

/* move all items to player inventory */
static bool loot(Inventory *player, Inventory *loot)
{
    bool success = false;
    if(loot->weapon_id > player->weapon_id)
    {
        success = true;
        player->weapon_id = loot->weapon_id;
        loot->weapon_id = 4;
    }
    if(loot->armor_id > player->armor_id)
    {
        success = true;
        player->armor_id = loot->armor_id;
        loot->armor_id = 0;
    }
    if(loot->heal_potion >0) /* just in case something goes wrong */
    {
        success = true;
        player->heal_potion += loot->heal_potion;
        loot->heal_potion = 0;
    }
    if(loot->money.gold > 0) /* just in case something goes wrong */
    {
        success = true;
        player->money.gold += loot->money.gold;
        loot->money.gold = 0;
    }
    if(loot->money.silver > 0) /* just in case something goes wrong */
    {
        success = true;
        player->money.silver += loot->money.silver;
        loot->money.silver = 0;
    }
    if(loot->money.copper > 0) /* just in case something goes wrong */
    {
        success = true;
        player->money.copper += loot->money.copper;
        loot->money.copper = 0;
    }
    if(loot->key_amount > 0)
    {
        success = true;
        for(int i = 0; i < loot->key_amount; i++)
        {
            player->keys[player->key_amount + i] = loot->keys[i];
        }
        player->key_amount += loot->key_amount;
        loot->key_amount = 0;
    }
    return success;
}

static bool have_key_with_id(const GameState *gamestate, int id)
{
    if(id == 0)
    {
        return true;
    }
    for(int k = 0; k < gamestate->player.inventory.key_amount; k++)
    {
        if(id == gamestate->player.inventory.keys[k])
        {
            return true;
        }
    }
    return false;
}

void level_up(Character *player)
{
    player->max_health += roll((Dice) {1, 10});
    player->actual_health = player->max_health;
    player->attack_mod++;
    player->inventory.key_amount = 0;
}

void interact(GameState *gamestate, const GameData *gamedata)
{
    const double interact_distance = 1.1;
    if(gamestate->player.animation_type == GESTURE && gamestate->player.animation_counter == 20)
    {
        /* look for chests and doors */
        for(int i = gamestate->player.position.y-1; i <= gamestate->player.position.y+1; i++)
        {
            for(int j = gamestate->player.position.x-1; j <= gamestate->player.position.x+1; j++)
            {
                /* door */
                if(gamestate->current_map[i][j].fieldtype == DOOR && distance(gamestate->player.position, (DoubleVector2) {j, i}) <= interact_distance)
                {
                    if(have_key_with_id(gamestate, gamestate->current_map[i][j].id))
                    {
                        /* the id of the final door is always 1 */
                        if(gamestate->current_map[i][j].id == 1)
                        {
                            gamestate->current_map_id++;
                            level_up(&gamestate->player);
                            if(gamedata->map_amount > gamestate->current_map_id)
                            {
                                load_map(gamestate, gamedata);
                            }
                            else
                            {
                                /*gamestate->current_map[i][j].text_timer = 50;
                                strcpy(gamestate->current_map[i][j].text, "END");*/
                                gamestate->current_map_id = 0;
                                load_map(gamestate, gamedata);
                                /* go back to main menu */
                                gamestate->state = MAIN_MENU;
                            }
                            save_the_game(gamestate);
                        }
                        else /* open or close the door */
                        {
                            gamestate->current_map[i][j].walkable = !gamestate->current_map[i][j].walkable;
                        }
                    }
                    else
                    {
                        gamestate->current_map[i][j].text_timer = 50;
                        sprintf(gamestate->current_map[i][j].text, "KEY %d", gamestate->current_map[i][j].id);
                    }
                }
                /* chest */
                if(gamestate->current_map[i][j].fieldtype == CHEST && distance(gamestate->player.position, (DoubleVector2) {j, i}) <= interact_distance)
                {
                    if(have_key_with_id(gamestate, gamestate->current_map[i][j].id))
                    {
                        gamestate->current_map[i][j].special.chest->open = true;
                        if(loot(&gamestate->player.inventory, &gamestate->current_map[i][j].special.chest->inventory))
                        {
                            gamestate->current_map[i][j].text_timer = 50;
                            strcpy(gamestate->current_map[i][j].text, "LOOT!");
                        }
                        else
                        {
                            gamestate->current_map[i][j].text_timer = 50;
                            strcpy(gamestate->current_map[i][j].text, "NO LOOT!");
                        }
                    }
                    else
                    {
                        gamestate->current_map[i][j].text_timer = 50;
                        sprintf(gamestate->current_map[i][j].text, "KEY %d", gamestate->current_map[i][j].id);
                    }
                }
            }
        }

        /* look for killed mobs */
        for(int i = 0; i < gamestate->current_enemy_amount; i++)
        {
            if(gamestate->current_enemy[i].actual_health <=0 && distance(gamestate->player.position, gamestate->current_enemy[i].position) <= interact_distance)
            {
                if(loot(&gamestate->player.inventory, &gamestate->current_enemy[i].inventory))
                {
                    gamestate->current_enemy[i].text_timer = 50;
                    strcpy(gamestate->current_enemy[i].text, "LOOT!");
                }
                else
                {
                    gamestate->current_enemy[i].text_timer = 50;
                    strcpy(gamestate->current_enemy[i].text, "NO LOOT!");
                }
            }
        }
    }
}

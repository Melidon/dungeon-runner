#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

#include "data.h"

void init(GameState *gamestate, GameData *gamedata, Graphics *graphics);

#endif // INIT_H_INCLUDED

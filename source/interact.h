#ifndef INTERACT_H_INCLUDED
#define INTERACT_H_INCLUDED

#include "data.h"

void interact(GameState *gamestate, const GameData *gamedata);

#endif // INTERACT_H_INCLUDED

#include <stdlib.h>

#include "data.h"
#include "endgame.h"

#include "debugmalloc.h"

void endgame(GameState *gamestate, GameData *gamedata, Graphics *graphics)
{
    /* free everything for gamedata */
    for(int i = 0; i < gamedata->map_amount; i++)
    {
        free(gamedata->map_name_list[i]);
    }
    free(gamedata->map_name_list);

    free(gamedata->weapon_list);
    free(gamedata->armor_list);
    free(gamedata->mob_list);

    /* free everything from gamestate */
    for(int i = 0; i < gamestate->current_map_size.y; i++)
    {
        for(int j = 0; j < gamestate->current_map_size.x; j++)
        {
            if(gamestate->current_map[i][j].fieldtype == CHEST)
            {
                free(gamestate->current_map[i][j].special.chest);
            }
        }
    }
    for(int i = 0; i < gamestate->current_map_size.y; i++)
    {
        free(gamestate->current_map[i]);
    }
    free(gamestate->current_map);
    free(gamestate->current_enemy);

    SDL_RemoveTimer(graphics->id);

    SDL_DestroyTexture(graphics->textures.tileset);
    SDL_DestroyTexture(graphics->textures.menu_background);
    SDL_DestroyTexture(graphics->textures.gold);
    SDL_DestroyTexture(graphics->textures.silver);
    SDL_DestroyTexture(graphics->textures.copper);
    SDL_DestroyTexture(graphics->textures.shield);
    SDL_DestroyTexture(graphics->textures.heart);
    for(int i = 0; i < gamedata->mob_amount+1; i++)
    {
        SDL_DestroyTexture(graphics->textures.animations[i]);
    }
    free(graphics->textures.animations);

    TTF_CloseFont(graphics->font);

    SDL_DestroyRenderer(graphics->renderer);
    SDL_DestroyWindow(graphics->window);
    IMG_Quit();
    SDL_Quit();
}

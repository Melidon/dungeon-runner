#ifndef LOAD_SAVE_H_INCLUDED
#define LOAD_SAVE_H_INCLUDED

#include "data.h"

void load_save(GameState *gamestate);

#endif // LOAD_SAVE_H_INCLUDED

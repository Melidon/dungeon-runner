7
name: Rat      health:  1, speed: 15, attack_mod:  4, damage_mod: -4, defense_mod:  4, weapon_id: 0, armor_id: 0, size: 32, link: images/characters/rat_animation.png
name: Snake    health:  1, speed: 15, attack_mod:  5, damage_mod:  0, defense_mod:  0, weapon_id: 1, armor_id: 0, size: 32, link: images/characters/snake_animation.png
name: Slime    health: 26, speed: 10, attack_mod:  3, damage_mod:  1, defense_mod: -5, weapon_id: 2, armor_id: 0, size: 32, link: images/characters/slime_animation.png
name: Goblin   health:  4, speed: 30, attack_mod:  1, damage_mod: -1, defense_mod:  2, weapon_id: 5, armor_id: 1, size: 32, link: images/characters/goblin_animation.png
name: Skeleton health:  6, speed: 30, attack_mod:  0, damage_mod:  0, defense_mod:  3, weapon_id: 3, armor_id: 0, size: 32, link: images/characters/skeleton_animation.png
name: Orc      health:  4, speed: 20, attack_mod:  3, damage_mod:  3, defense_mod:  0, weapon_id: 7, armor_id: 2, size: 32, link: images/characters/orc_animation.png
name: Minotaur health: 39, speed: 30, attack_mod:  9, damage_mod:  4, defense_mod:  4, weapon_id: 8, armor_id: 0, size: 48, link: images/characters/minotaur_animation.png
